import * as user from './modules/user'
import * as role from './modules/role'
import * as resource from './modules/resource'
import * as signDoc from './modules/signDoc'
import * as sysLog from './modules/sysLog'
import * as common from './modules/common'
import * as sysconfig from './modules/sysconfig'
import * as signerq from './modules/signreq'

export default {
  common,      //登录
  user,       //用户
  role,       //角色
  resource,   //资源
  signDoc,    //文件
  sysLog,     //系统日志
  sysconfig,  //系统配置
  signerq,    //签名请求
}
