import request from '../http'
import requestUrl from '../requestUrl'
import requestParam from '../requestParam'
import isInteger from 'lodash/isInteger'

//获取用户列表
export function list(params) {
  return request({
    url: requestUrl('/user/list'),
    method: 'get',
    params: requestParam(params, this.method)
  })
}

/**
 * 添加用户
 * @param params
 * @returns {AxiosPromise<any>}
 */
export function add(params) {
  // return request({
  //   url:requestUrl('/user/add'),
  //   method:'post',
  //   params:requestParam(params,this.method)
  // });
  return request.post(requestUrl('/user/add'), params);
}

/**
 * 更新用户
 * @param params
 * @returns {AxiosPromise<any>}
 */
export function modify(params) {
  // return request({
  //   url:requestUrl('/user/modify'),
  //   method:'post',
  //   params:params
  // });
  return request.post(requestUrl('/user/modify'), params);
}

/**
 * 批量授权
 * @param params
 * @returns {AxiosPromise<any>}
 */
export function modifyBatch(params) {
  // return request({
  //   url:requestUrl('/user/list/modify'),
  //   method:'post',
  //   params:params
  // });
  return request.post(requestUrl('/user/list/modify'), params);
}

/**
 * 获取当前用户信息
 * @returns {AxiosPromise}
 */
export function info() {
  return request({
    url: requestUrl('/user/info'),
    method: 'get'
  });
}

/**
 *
 * 根据id查看角色信息
 * @param params
 * @returns
 */
export function FindByid(id) {
  return request({
    url: requestUrl('/user/FindByid/' + (isInteger(id) ? id : '')),
    method: 'get'
  });
}

/**
 *
 * 删除用户
 */
export function remove(params) {
  return request.post(requestUrl('/user/remove'), params);
}


