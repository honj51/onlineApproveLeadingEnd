import request from '../http'
import requestUrl from '../requestUrl'
import requestParam from '../requestParam'
import isInteger from 'lodash/isInteger'

/**
 * 角色列表
 */
export function select() {
  return request({
    url:requestUrl('/role/select'),
    method:'get'
  });
}

/**
 * 角色分页
 * @param params
 * @returns {AxiosPromise}
 */
export function list(params) {
  return request({
    url:requestUrl('/role/list'),
    method:'get',
    params:requestParam(params)
  });
}

/**
 * 批量删除角色
 * @param params
 * @returns {AxiosPromise<any>}
 */
export function removeBatch(params) {
  return request.post(requestUrl('/role/removeBatchByRoleIdList'),params);
}

/**
 * 获取角色下的资源
 * @param id
 * @returns {AxiosPromise}
 */
export function info(id) {
  return request({
    url:requestUrl('/role/info' + (isInteger(id) ? `/${id}` : '')),
    method:'get'
  });
}

/**
 * 角色增加
 * @param params
 * @returns {AxiosPromise<any>}
 */
export function add(params) {
  return request.post(requestUrl('/role/add'),params);
}

/**
 * 角色更新
 * @param params
 * @returns {AxiosPromise<any>}
 */
export function modify(params) {
  return request.post(requestUrl('/role/modify'),params);
}
