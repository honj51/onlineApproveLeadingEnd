import request from '../http'
import requestUrl from '../requestUrl'
import requestParam from '../requestParam'
import isInteger from 'lodash/isInteger'

/**
 * 根据id查找某个文件具体签名信息
 */
export function list(param) {
  return request({
    url:requestUrl('/signreq/list'),
    method:'get',
    params:requestParam(param)
  });
}
