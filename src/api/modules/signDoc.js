import request from '../http'
import requestUrl from '../requestUrl'
import requestParam from '../requestParam'
import isInteger from 'lodash/isInteger'

/**
 * 新增文件
 */
export function add(params) {
  return request.post(requestUrl('/signDoc/add'),params);
}


/**
 * 根据id查找某个文件具体信息
 * @param id
 * @returns {AxiosPromise}
 */
export function info(id) {
  return request({
    url:requestUrl('/signDoc/info'+isInteger(id)?`/${id}`:''),
    method:'get'
  });
}

/**
 * 查询列表
 */
export function list(params) {
  return request({
    url:requestUrl('/signDoc/list'),
    method:'get',
    params:params
  });
}

/**
 * /signDoc/lists
 根据持有者id查询列表
 */
export function lists(params) {
  return request({
    url:requestUrl('/signDoc/lists'),
    method:'get',
    params:params
  });
}

/**
 * 修改文件信息
 * @param params
 * @returns {AxiosPromise<any>}
 */
export function modify(params) {
  return request.post(requestUrl('/signDoc/modify'),params);
}

/**
 * 删除文件
 */
export function remove(params) {
  return request.post(requestUrl('/signDoc/remove'),params);
}
