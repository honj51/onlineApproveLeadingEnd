import request from '../http'
import requestUrl from '../requestUrl'
import requestParam from '../requestParam'
import isInteger from 'lodash/isInteger'

/**
 * 根据id查找详情
 * @param id
 */
export function info(id) {
  return request({
    url:requestUrl('/syslog/info'+isInteger(id)?`/${id}`:''),
    method:'get'
  });
}

/**
 * 查询列表
 * @returns {AxiosPromise}
 */
export function list(param) {
  return request({
    url:requestUrl('/syslog/list'),
    method:'get',
    params:requestParam(param)
  });
}


/**
 * 批量删除
 * @param params
 * @returns {AxiosPromise<any>}
 */
export function remove(params) {
  return request.post(requestUrl('/syslog/remove'),params);
}
