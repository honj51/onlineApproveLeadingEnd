import request from '../http'
import requestUrl from '../requestUrl'
import requestParam from '../requestParam'

/**
 * 获取验证码
 */
export function captcha(uuid) {
  return requestUrl(`/captcha.jpg?uuid=${uuid}`);
}

/**
 * 登录
 * @param params
 * @returns {AxiosPromise<any>}
 */
export function login(params) {
  return request.post(requestUrl('/login'),params);
}
