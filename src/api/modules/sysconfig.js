import request from '../http'
import requestUrl from '../requestUrl'
import requestParam from '../requestParam'

/**
 * 添加
 * @param params
 * @returns {AxiosPromise}
 */
export function add(params) {
  return request.post(requestUrl("/sysconfig/add"),params);
}

/**
 * 查询信息
 * @param id
 * @returns {AxiosPromise}
 */
export function info(id) {
  return request({
    url: requestUrl(`/sysconfig/info/${id}`),
    method:'get'
  })
}

/**
 * 列表
 * @param params
 * @returns {AxiosPromise}
 */
export function list(param) {
  return request({
    url:requestUrl("/sysconfig/list"),
    method:'get',
    params:requestParam(param)
  })
}

/**
 * 修改更新
 * @param param
 * @returns {AxiosPromise<any>}
 */
export function modify(param) {
  return request.post(requestUrl("/sysconfig/modify"),param);
}

/**
 * 批量删除
 * @param idList
 * @returns {AxiosPromise<any>}
 */
export function removeBatch(idList) {
  return request.post(requestUrl("/sysconfig/remove"),idList);
}
