import request from '../http'
import requestUrl from '../requestUrl'
import requestParam from '../requestParam'
import isInteger from 'lodash/isInteger'

/**
 * 资源树结构
 * @returns {AxiosPromise}
 */
export function nav() {
  return request({
    url:requestUrl('/resource/nav'),
    method:'get',
  });
}

/**
 * 添加资源
 * @param params
 * @returns {AxiosPromise<any>}
 */
export function add(params) {
  return request.post(requestUrl('/resource/add'),params);
}

/**
 * 更新资源
 * @param params
 * @returns {AxiosPromise<any>}
 */
export function modify(params) {
  return request.post(requestUrl('/resource/modify'),params);
}

/**
 * 删除菜单
 */
export function deleteit(params){
  return request({
    url:requestUrl('/resource/delete/'+params),
    method:'get',
  });
}
