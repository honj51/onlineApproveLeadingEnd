import Vue from 'vue'
import Vuex from 'vuex'
import user from './user'

import mutations from './mutations'


Vue.use(Vuex);

const state = {
  // 导航条, 布局风格, defalut(默认) / inverse(反向)
  navbarLayoutType: 'default',
  // 侧边栏, 布局皮肤, light(浅色) / dark(黑色)
  sidebarLayoutSkin: 'light',
  // 侧边栏, 水平折叠收起状态
  sidebarCollapse: false,
  // 页面文档可视高度(随窗口改变大小)
  documentClientHeight: 0,
  // 菜单导航
  menuNavList: [],
  // 内容区域tabs标签页
  contentTabs: [],
  contentTabsActiveName: '',
  token:null  //登录成功后获取的token
};

export default new Vuex.Store({
  state,
  mutations,
  modules:{
    user
  }
});
