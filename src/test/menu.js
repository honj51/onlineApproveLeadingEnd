export function getMenuList() {
  return menuList;
}
const menuList = [
  {
    "menuId": 1,
    "parentId": 0,
    "parentName": null,
    "name": "系统管理",
    "url": null,
    "perms": null,
    "type": 0,
    "icon": "fa fa-cog",
    "orderNum": 0,
    "open": null,
    "list":[{
      "menuId": 4,
      "parentId": 1,
      "parentName": null,
      "name": "角色管理",
      "url": "modules/sys/roleManage.html",
      "perms": null,
      "type": 1,
      "icon": "fa fa-user-secret",
      "orderNum": 1,
      "open":null,
    },{
      "menuId": 5,
      "parentId": 1,
      "parentName": null,
      "name": "系统日志",
      "url": "modules/sys/sysLog.html",
      "perms": null,
      "type": 1,
      "icon": "fa fa-file-text-o",
      "orderNum": 1,
      "open":null,
    },{
      "menuId": 6,
      "parentId": 1,
      "parentName": null,
      "name": "菜单管理",
      "url": "modules/sys/menu.html",
      "perms": null,
      "type": 1,
      "icon": "fa fa-th-list",
      "orderNum": 1,
      "open":null,
    },{
    "menuId": 7,
    "parentId": 1,
    "parentName": null,
    "name": "用户角色",
    "url": "modules/sys/userRole.html",
    "perms": null,
    "type": 1,
    "icon": "fa fa-user-secret",
    "orderNum": 1,
    "open":null,
}]
  },{
    "menuId": 2,
    "parentId": 0,
    "parentName": null,
    "name": "业务管理",
    "url": null,
    "perms": null,
    "type": 0,
    "icon": "fa fa-cog",
    "orderNum": 0,
    "open": null,
    "list": [{
        "menuId": 3,
        "parentId": 2,
        "parentName": null,
        "name": "组织机构管理",
        "url": 'modules/sys/organics.html',
        "perms": null,
        "type": 1,
        "icon": "fa fa-user",
        "orderNum": 1,
        "open": null,
        "list": null
    },]
  }
];


