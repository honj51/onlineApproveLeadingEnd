export function getAdminMenuList() {
  return adminMenuList;
}

export function getNormalMenuList() {
  return normalUserMenuList;
}

const adminMenuList = [{
  "menuId": 1,
  "parentId": 0,
  "parentName": null,
  "name": "机构流程管理",
  "url": "modules/sys/processManage.html",
  "perms": null,
  "type": 1,
  "icon": "fa fa-retweet",
  "orderNum": 1,
},{
  "menuId": 2,
  "parentId": 0,
  "parentName": null,
  "name": "用户管理",
  "url": "modules/sys/userManage.html",
  "perms": null,
  "type": 1,
  "icon": "fa fa-users",
  "orderNum": 2,
},{
  "menuId": 3,
  "parentId": 0,
  "parentName": null,
  "name": "角色管理",
  "url": "modules/sys/roleManage.html",
  "perms": null,
  "type": 1,
  "icon": "fa fa-user-secret",
  "orderNum": 3,
},{
  "menuId": 4,
  "parentId": 0,
  "parentName": null,
  "name": "系统日志",
  "url": "modules/sys/sysLog.html",
  "perms": null,
  "type": 1,
  "icon": "fa fa-calendar-o",
  "orderNum": 4,
},{
  "menuId": 5,
  "parentId": 0,
  "parentName": null,
  "name": "个人中心",
  "url": "modules/sys/personInfo.html",
  "perms": null,
  "type": 1,
  "icon": "fa fa-address-card-o",
  "orderNum": 5,
},{
  "menuId": 6,
  "parentId": 0,
  "parentName": null,
  "name": "系统参数",
  "url": "modules/sys/sysconfig.html",
  "perms": null,
  "type": 1,
  "icon": "fa fa-cog",
  "orderNum": 6,
}];

const normalUserMenuList = [{
  "menuId":1,
  "name": "审批流程",
  "url": 'modules/sys/process.html',
  "icon": "fa fa-retweet"
},{
  "menuId":2,
  "name": "签名文件",
  "url": 'modules/sys/files.html',
  "icon": "fa fa-file-pdf-o"
},{
  "menuId":3,
  "name": "个人中心",
  "url": 'modules/sys/personInfo.html',
  "icon": "fa fa-address-card-o"
}];
