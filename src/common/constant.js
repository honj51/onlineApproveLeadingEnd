/**
 * Created by tlm on 2018/1/4 下午12:06.
 * 常量定义
 */

const USER_AVATAR = '/static/images/user.jpg'; //默认用户头像
const PAGE_SIZE = 10; //默认分页大小
const PAGE_SIZES=[5,10, 20, 50,100];//分页大小选择

export default
{
    USER_AVATAR,
    PAGE_SIZE,
    PAGE_SIZES
}
