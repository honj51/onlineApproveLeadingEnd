import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

export default new Router({
  mode:'hash',
  routes:[
    {path:'/',redirect:'/login'},
    {path:'/404',component:()=>import('@/views/error/404.vue'),name:'404',desc:'404未找到'},
    {path:'/login',component:()=>import('@/views/login.vue'),name:'login',desc:'登录',meta:{allowBack:false}},
    {
      path:'/onlineapprove',
      component:()=>import('@/views/layout/index.vue'),
      name:'layout',
      redirect:{name:'home'},
      desc:'上左右整体布局',
      children:[
        {path:'/home',component:()=>import('@/views/home/home.vue'),name:'home',desc:'首页'},
        {path:'/processManage',component:()=>import('@/views/processManage/index'),name:'processManage',desc:'审批流程管理',meta:{isTab:true}},
        {path:'/roleManage',component:()=>import('@/views/roleManage/index'),name:'roleManage',desc:'角色管理',meta:{isTab:true}},
        {path:'/userManage',component:()=>import('@/views/userManage/index'),name:'userManage',desc:'用户管理',meta:{isTab:true}},
        {path:'/sysLog',component:()=>import('@/views/syslog/index'),name:'sysLog',desc:'系统日志',meta:{isTab:true}},
        {path:'/personInfo',component:()=>import('@/views/personInfo/index'),name:'personInfo',desc:'个人中心',meta:{isTab:true}},
        {path:'/sysconfig',component:()=>import('@/views/sysconfig/index'),name:'sysconfig',desc:'系统参数',meta:{isTab:true}},

        {path:'/process',component:()=>import('@/views/process/index'),name:'process',desc:'审批流程',meta:{isTab:true}},
        {path:'/files',component:()=>import('@/views/files/index'),name:'files',desc:'签名文件',meta:{isTab:true}},

        {path:'/signPDF',component:()=>import('@/views/sign/index'),name:'sign',desc:'签章',meta:{isTab:false}},
        // {path:'/menu',component:()=>import('@/views/menu/index'),name:'menu',desc:'菜单管理',meta:{isTab:true}},

        {path:'/guofu/xuegong/excuse',component:()=>import('@/views/template/guofu/xuegong/excuse'),name:'excuse',desc:'请假流程',meta:{isTab:false}},
      ],
      beforeEnter (to,from,next){
        let token = Vue.cookie.get('token');
        if(!token){
          next({name:'login',query:{redirect:to.fullPath}});
          let allowBack = true;
          if(to.meta.allowBack!==undefined){
            allowBack = to.meta.allowBack;
          }
          if(!allowBack){
            history.pushState(null,null,location.href);
          }
        }else{
          next();
        }
      }
    },
    {path:'*',redirect:{name:'404'}}
  ]
});
